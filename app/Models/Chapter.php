<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = 'chapters';

    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'serie_id',
        'name',
        'chapter_url',
        'duration',
        'description',
    ];

    public function serie()
    {
        return $this->belongsTo(Serie::class);
    }
}
