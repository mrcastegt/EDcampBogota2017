-- SE CREA LA TABLA DE HISTÓRICO DE VISTAS
CREATE TABLE historicals (
  id SERIAL NOT NULL,
  user_id INT NOT NULL,
  chapter_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP,
  CONSTRAINT historicals_id_pk PRIMARY KEY (id),
  CONSTRAINT historicals_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE RESTRICT ON DELETE RESTRICT ,
  CONSTRAINT historicals_chapter_id_fk FOREIGN KEY (chapter_id) REFERENCES chapters (id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

COMMENT ON TABLE historicals IS 'Histórico de vistas de los capítulos por cada usuario';
